defmodule Aoc21.Day3 do
  use Bitwise

  def bit_at(int, n) when is_number(int) do
    shift = 12 - n
    int >>> shift &&& 1
  end
  def bit_at(ints, n) when is_list(ints) do
    Enum.map(ints, &(bit_at(&1, n)))
  end

  def gamma_at(data, n) do
    case data |> bit_at(n) |> Enum.frequencies() do
      %{0 => zeros, 1 => ones} when zeros > ones -> 0
      %{0 => zeros, 1 => ones} when zeros < ones -> 1
    end
  end

  def epsilon_at(data, n) do
    case data |> bit_at(n) |> Enum.frequencies() do
      %{0 => zeros, 1 => ones} when zeros > ones -> 1
      %{0 => zeros, 1 => ones} when zeros < ones -> 0
    end
  end

  def number_by(data, func) do
    <<n::12>> = 1..12 |> Enum.map(&(func.(data, &1)))
    |> Enum.into(<<>>, fn bit -> <<bit::1>> end)
    n
  end

  def gamma(data) do
    data |> number_by(&gamma_at/2)
  end

  def epsilon(data) do
    data |> number_by(&epsilon_at/2)
  end

  def power_consumption(gamma, epsilon) do
    gamma * epsilon
  end

  def o2gen_rating(data) do
    Enum.reduce_while(1..12, data, fn n, acc ->
      filter_bit = case acc |> bit_at(n) |> Enum.frequencies() do
        %{0 => zeros, 1 => ones} when zeros > ones -> 0
        %{0 => zeros, 1 => ones} when zeros < ones -> 1
        %{0 => zeros, 1 => ones} when zeros == ones -> 1
      end
      number_list = Enum.filter(acc, fn num ->
        bit_at(num, n) == filter_bit
      end)
      if length(number_list) > 1, do: {:cont, number_list}, else: {:halt, number_list}
    end)
    |> hd
  end

  def co2scrub_rating(data) do
    Enum.reduce_while(1..12, data, fn n, acc ->
      filter_bit = case acc |> bit_at(n) |> Enum.frequencies() do
                     %{0 => zeros, 1 => ones} when zeros > ones -> 1
                     %{0 => zeros, 1 => ones} when zeros < ones -> 0
                     %{0 => zeros, 1 => ones} when zeros == ones -> 0
                     _ -> :error
                   end
      number_list = Enum.filter(acc, fn num ->
        bit_at(num, n) == filter_bit
      end)
      if length(number_list) > 1, do: {:cont, number_list}, else: {:halt, number_list}
    end)
    |> hd
  end

  def solve1(input) do
    data = input |> parse_input()
    gamma = data |> gamma()
    epsilon = data |> epsilon()
    power_consumption(gamma, epsilon)
  end

  def solve2(input) do
    data = input |> parse_input()
    o2 = data |> o2gen_rating()
    co2 = data |> co2scrub_rating()
    o2 * co2
  end

  def parse_input(str) do
    String.split(str, "\n")
    |> Enum.reject(fn str -> String.trim(str) == "" end)
    |> Enum.map(fn str ->
      {int, ""} = Integer.parse(str, 2)
      int
    end)
  end
end
