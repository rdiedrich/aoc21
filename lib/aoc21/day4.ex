defmodule Aoc21.Day4 do
  defmodule BingoBoard do
    defstruct fields: [], marked: []
  end

  def mark_board(%BingoBoard{marked: []} = board, n),
    do: %BingoBoard{fields: board.fields, marked: [n]}

  def mark_board(board, n),
    do: %BingoBoard{fields: board.fields, marked: [n] ++ board.marked}

  def play(%{numbers: numbers, boards: boards}) do
    for n <- numbers do
      boards |> Enum.map(fn b -> mark_board(b, n) end)
    end
  end

  def solve1(input) do
    input
    |> parse_input()
    |> play()
    |> Enum.take(-2)
    |> IO.inspect()
  end

  def solve2(input) do
    input |> parse_input()
  end

  def parse_input(string) do
    with [numbers_string | boards_string] <- string |> String.split("\n\n"),
         numbers =
           numbers_string
           |> String.split(",")
           |> Enum.map(&String.to_integer/1),
         boards =
           Enum.map(boards_string, fn line ->
             %BingoBoard{fields: line |> String.split() |> Enum.map(&String.to_integer/1)}
           end) do
      %{numbers: numbers, boards: boards}
    end
  end
end
