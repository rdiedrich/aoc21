defmodule Aoc21.Day2 do


  defp move({:forward, val}, %{horizontal: horizontal, depth: _depth} = position) do
    %{position | horizontal: horizontal + val}
  end

  defp move({:up, val}, %{horizontal: _horizontal, depth: depth} = position) do
    %{position | depth: depth - val}
  end

  defp move({:down, val}, %{horizontal: _horizontal, depth: depth} = position) do
    %{position | depth: depth + val}
  end

  defp dive(new_depth) do
    if new_depth < 0, do: 0, else: new_depth
  end

  defp move2({:forward, val}, %{horizontal: horizontal, depth: depth, aim: aim} = position) do
    %{position | horizontal: horizontal + val, depth: dive(depth + aim * val)}
  end

  defp move2({:up, val}, %{horizontal: _horizontal, depth: _depth, aim: aim} = position) do
    %{position | aim: aim - val}
  end

  defp move2({:down, val}, %{horizontal: _horizontal, depth: _depth, aim: aim} = position) do
    %{position | aim: aim + val}
  end

  def solve1(input) do
    input |> parse_input()
    |> Enum.reduce(%{horizontal: 0, depth: 0}, fn cmd, pos -> move(cmd, pos) end)
  end

  def solve2(input) do
    input |> parse_input()
    |> Enum.reduce(%{horizontal: 0, depth: 0, aim: 0}, fn cmd, pos -> move2(cmd, pos) end)
  end

  def parse_input(str) do
    String.split(str, "\n")
    |> Enum.reject(fn str -> String.trim(str) == "" end)
    |> Enum.map(fn str ->
      case String.split(str, " ") do
        [cmd, val] -> {String.to_atom(cmd), String.to_integer(val)}
      end
    end)
  end
end
