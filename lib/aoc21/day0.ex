defmodule Aoc21.Day0 do
  def solve1(input) do
    input |> parse_input()
  end

  def solve2(input) do
    input |> parse_input()
  end

  def parse_input(string) do
    string
    |> Stream.map(&String.to_integer/1)
  end
end
