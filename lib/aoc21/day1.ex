defmodule Aoc21.Day1 do
  defp count_increases(list) when is_list(list) do
    list
    # we want to compare every value with its successor
    |> Enum.chunk_every(2, 1, :discard)
    # only keep increases in the list
    |> Enum.filter(fn [a, b] -> b > a end)
    # count how many increases we have
    |> length()
  end

  def solve1(input) do
    input |> parse_input() |> Enum.to_list()
    |> count_increases()
  end

  def solve2(input) do
    input |> parse_input()
    # take the sum of three consecutive values
    |> Enum.chunk_every(3, 1, :discard)
    |> Enum.map(&Enum.sum/1)
    # then compare like in part 1
    |> count_increases()
  end

  def parse_input(string) do
    string
    |> String.split()
    |> Stream.map(&String.to_integer/1)
    |> Enum.to_list()
  end
end
